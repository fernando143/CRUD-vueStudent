import firebase from 'firebase'
 const config = {
    apiKey: "AIzaSyBVH-w6sllI-97qb8E95C3axwyxViE8ng0",
    authDomain: "crud-vue-firebase-b6b3e.firebaseapp.com",
    databaseURL: "https://crud-vue-firebase-b6b3e.firebaseio.com",
    projectId: "crud-vue-firebase-b6b3e",
    storageBucket: "crud-vue-firebase-b6b3e.appspot.com",
    messagingSenderId: "886282180753"
},
    firebaseapp = firebase.initializeApp(config)
    
    export const db = firebaseapp.database()
    export const dbRef = db.ref('vueStudents')