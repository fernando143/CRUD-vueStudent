import Vue from 'vue'
import Vuex from 'vuex'

const state = {
    data:{
        showAddModal: false,
        showEditModal: false,
        showDeleteModal: false,
        errorMessage: '',
        successMessage: '',
        students: [],
        activeStudent: {}
    }
    
}

const getters = {
    getData: state => state.data,

    getShowAddModal: state => state.data.showAddModal,
    getShowEditModal: state => state.data.showEditModal,
    getShowDeleteModal: state => state.data.showDeleteModal,

    getErrorMessage: state => state.data.errorMessage,
    getSuccessMessage: state => state.data.successMessage,

    getActiveStudent: state => state.data.activeStudent
}

const mutations = {
    TOGGLE_MODAL: (state, payload) => {        
        switch (payload) {
            case 'add':
                state.data.showAddModal = !state.data.showAddModal                
                break;
            case 'edit':
                state.data.showEditModal = !state.data.showEditModal                
                break;
            case 'delete':
                state.data.showDeleteModal = !state.data.showDeleteModal
                break;
        }
    },
    SET_MESSAGES: (state, payload) => {
        switch (payload.name) {
            case 'addStudent':
                if (payload.value) {
                    state.data.errorMessage = payload.message                    
                }else{
                    state.data.successMessage = payload.message
                }
                break;
            case 'editStudent':
                if(payload.value){
                    state.data.errorMessage = payload.message                    
                }else{
                    state.data.successMessage = payload.message
                }
                break;
            case 'deleteStudent':
                if (payload.value) {
                    state.data.errorMessage = payload.message
                } else {
                    state.data.successMessage = payload.message
                }
                break;
        }
       

        setTimeout(() => {
            state.data.errorMessage = ''
            state.data.successMessage = ''
        }, 2500);
    },
    ACTIVE_STUDENT: (state, payload) => {
        state.data.activeStudent = payload
    }
}

const actions = {
    toggleModal: (context, payload) => {
        context.commit('TOGGLE_MODAL', payload)
    },
    setMessages: (context, payload) => {
        context.commit('SET_MESSAGES', payload)
    },
    activeStudent: (context, payload) => {
        context.commit('ACTIVE_STUDENT', payload)
    }
}

Vue.use(Vuex)

export const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})