import Vue from 'vue'
//import Vuex from 'vuex'
import VueFire from 'vuefire'
import App from './App.vue'
import {store} from './helpers/store'

Vue.use(VueFire)

new Vue({
  el: '#app',
  render: h => h(App),
  store
})
